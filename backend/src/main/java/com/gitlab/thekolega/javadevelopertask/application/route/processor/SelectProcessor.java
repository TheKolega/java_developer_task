package com.gitlab.thekolega.javadevelopertask.application.route.processor;

import com.gitlab.thekolega.javadevelopertask.application.dto.CandidateDTO;
import org.apache.camel.Exchange;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class SelectProcessor implements org.apache.camel.Processor {

    @Override
    public void process(Exchange exchange) throws Exception {

        //the camel jdbc select query has been executed. We get the list of candidates.
        ArrayList<Map<String, Object>> dataList = (ArrayList<Map<String, Object>>) exchange.getIn()
                .getBody();
        List<CandidateDTO> candidateDTOList = new ArrayList<>();
        //
        // System.out.println(dataList);
        for (Map<String, Object> data : dataList) {
            CandidateDTO candidateDTO = new CandidateDTO();
            candidateDTO.setId((Long) data.get("id"));
            candidateDTO.setFirstName((String) data.get("first_name"));
            candidateDTO.setLastName((String) data.get("last_name"));
            candidateDTO.setUniqueCitizenId((String) data.get("unique_citizen_id"));
            candidateDTO.setYearOfBirth(((Integer) data.get("year_of_birth")).shortValue());
            candidateDTO.setEmail((String) data.get("email"));
            candidateDTO.setPhone((String) data.get("phone"));
            candidateDTO.setComment((String) data.get("comment"));
            candidateDTO.setHired((boolean) data.get("is_hired"));
            candidateDTO.setDateEdited(((Date) data.get("date_edited")).toLocalDate());
            candidateDTOList.add(candidateDTO);
        }
        exchange.getIn().setBody(candidateDTOList);

    }

}
