package com.gitlab.thekolega.javadevelopertask.application.dto;

import com.gitlab.thekolega.javadevelopertask.domain.Candidate;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

@Component
public class CandidateMapper {

    public CandidateDTO convertToDTO(Candidate entity) {
        CandidateDTO dto = new CandidateDTO();
        BeanUtils.copyProperties(entity, dto);
        return dto;
    }

    public Candidate convertToEntity(CandidateDTO dto) {
        Candidate entity = new Candidate();
        BeanUtils.copyProperties(dto, entity);
        return entity;
    }

    public Candidate convertToEntity(CandidateDTOCreate dto) {
        Candidate entity = new Candidate();
        BeanUtils.copyProperties(dto, entity);
        return entity;
    }

    public Candidate convertToEntity(CandidateDTOEdit dto) {
        Candidate entity = new Candidate();
        BeanUtils.copyProperties(dto, entity);
        return entity;
    }

}
