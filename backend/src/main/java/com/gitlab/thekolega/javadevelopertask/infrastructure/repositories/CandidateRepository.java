package com.gitlab.thekolega.javadevelopertask.infrastructure.repositories;

import com.gitlab.thekolega.javadevelopertask.domain.Candidate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CandidateRepository extends JpaRepository<Candidate, Long> {
}
