package com.gitlab.thekolega.javadevelopertask.application.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Getter
@Setter
public class CandidateDTOCreate {

    @Size(max = 64, message = "First name must be shorter than 64 characters")
    private String firstName = "firstNameValue";

    @Size(max = 64, message = "Last name must be shorter than 64 characters")
    private String lastName = "lastNameValue";

    @Size(max = 16, message = "Unique Citizen ID must be shorter than 16 characters")
    private String uniqueCitizenId = "0101000123456";

    @Range(min = 1000, max = 9999, message = "Year must be between 1000 and 9999")
    private Short yearOfBirth = 1000;

    @Email
    @Size(max = 320, message = "Email must be shorter than 320 characters.")
    private String email = "e@mail.com";

    @Size(max = 16, message = "Phone number must be shorter than 16 characters.")
    private String phone = "+381223334444";

    @Size(max = 255, message = "Comment must be shorter than 255 characters.")
    private String comment = "commentValue";

    @NotNull(message = "Field 'Is hired:' must not be empty")
    private boolean isHired = false;

    @JsonIgnore
    private LocalDate dateEdited = LocalDate.now();

}
