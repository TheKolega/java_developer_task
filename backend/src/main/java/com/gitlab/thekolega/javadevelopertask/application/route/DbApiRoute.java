package com.gitlab.thekolega.javadevelopertask.application.route;

import com.gitlab.thekolega.javadevelopertask.application.route.processor.InsertProcessor;
import com.gitlab.thekolega.javadevelopertask.application.route.processor.SelectProcessor;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;

@Component
public class DbApiRoute extends RouteBuilder {

    @Autowired
    DataSource dataSource;

    @Autowired
    InsertProcessor insertProcessor;

    @Autowired
    SelectProcessor selectProcessor;

    @Override
    public void configure() throws Exception {

        //Insert Route
        from("direct:insert")
                .process(insertProcessor)
                .to("jdbc:dataSource");

        // Select Route
        from("direct:select")
                .setBody(constant("select * from candidate order by id asc"))
                .to("jdbc:dataSource")
                .process(selectProcessor);

    }

}
