package com.gitlab.thekolega.javadevelopertask.application.dto;

import lombok.Getter;
import lombok.Setter;
import org.apache.camel.dataformat.bindy.annotation.CsvRecord;
import org.apache.camel.dataformat.bindy.annotation.DataField;
import org.apache.camel.dataformat.bindy.annotation.KeyValuePairField;
import org.apache.camel.dataformat.bindy.annotation.Section;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Getter
@Setter
@CsvRecord(isOrdered = true, separator = ",")
@Section(number = 1)
public class CandidateDTO {

    @DataField(pos = 1, position = 1)
    @KeyValuePairField(tag = 1, position = 1)
    private Long id;

    @Size(max = 64, message = "First name must be shorter than 64 characters")
    @DataField(pos = 2, position = 2)
    @KeyValuePairField(tag = 2, position = 2)
    private String firstName;

    @Size(max = 64, message = "Last name must be shorter than 64 characters")
    @DataField(pos = 3, position = 3)
    @KeyValuePairField(tag = 3, position = 3)
    private String lastName;

    @Size(max = 16, message = "Unique Citizen ID must be shorter than 16 characters")
    @DataField(pos = 4, position = 4)
    @KeyValuePairField(tag = 4, position = 4)
    private String uniqueCitizenId;

    @Range(min = 1000, max = 9999, message = "Year must be between 1000 and 9999")
    @DataField(pos = 5, position = 5)
    @KeyValuePairField(tag = 5, position = 5)
    private Short yearOfBirth;

    @Email
    @Size(max = 320, message = "Email must be shorter than 320 characters.")
    @DataField(pos = 6, position = 6)
    @KeyValuePairField(tag = 6, position = 6)
    private String email;

    @Size(max = 16, message = "Phone number must be shorter than 16 characters.")
    @DataField(pos = 7, position = 7)
    @KeyValuePairField(tag = 7, position = 7)
    private String phone;

    @Size(max = 255, message = "Comment must be shorter than 255 characters.")
    @DataField(pos = 8, position = 8)
    @KeyValuePairField(tag = 8, position = 8)
    private String comment;

    @NotNull(message = "Field 'Is hired:' must not be empty")
    @DataField(pos = 9, position = 9)
    @KeyValuePairField(tag = 9, position = 9)
    private boolean isHired;

    @NotNull(message = "Field 'Time edited:' must not be empty")
    @DataField(pos = 10, position = 10, pattern = "yyyy-MM-dd")
    @KeyValuePairField(tag = 10, position = 10)
    private LocalDate dateEdited = LocalDate.now();

}
