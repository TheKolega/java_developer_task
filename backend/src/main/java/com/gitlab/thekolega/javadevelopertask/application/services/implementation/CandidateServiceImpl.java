package com.gitlab.thekolega.javadevelopertask.application.services.implementation;


import com.gitlab.thekolega.javadevelopertask.application.dto.CandidateDTO;
import com.gitlab.thekolega.javadevelopertask.application.dto.CandidateDTOCreate;
import com.gitlab.thekolega.javadevelopertask.application.dto.CandidateDTOEdit;
import com.gitlab.thekolega.javadevelopertask.application.dto.CandidateMapper;
import com.gitlab.thekolega.javadevelopertask.application.services.CandidateService;
import com.gitlab.thekolega.javadevelopertask.domain.Candidate;
import com.gitlab.thekolega.javadevelopertask.infrastructure.repositories.CandidateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CandidateServiceImpl implements CandidateService {

    @Autowired
    private CandidateRepository candidateRepository;

    @Autowired
    private CandidateMapper candidateMapper;

    @Override
    public CandidateDTO create(@Validated CandidateDTOCreate candidateDTO) {
        Candidate candidate = candidateMapper.convertToEntity(candidateDTO);
        candidate.setId(null);
        candidate.setDateEdited(LocalDate.now());
        return candidateMapper.convertToDTO(candidateRepository.save(candidate));
    }

    @Override
    public CandidateDTO findOne(Long id) throws EntityNotFoundException {
        Optional<Candidate> candidateOptional = candidateRepository.findById(id);
        if (!candidateOptional.isPresent()) {
            throw new EntityNotFoundException("No candidate with id " + id + " found.");
        }
        return candidateMapper.convertToDTO(candidateOptional.get());
    }

    @Override
    public List<CandidateDTO> findAll() {
        return candidateRepository.findAll().stream().map(i -> candidateMapper.convertToDTO(i)).collect(Collectors.toList());
    }

    @Override
    public CandidateDTO update(Long id, @Validated CandidateDTOEdit dto) throws EntityNotFoundException {
        Optional<Candidate> candidateOptional = candidateRepository.findById(id);
        if (!candidateOptional.isPresent()) {
            throw new EntityNotFoundException("No candidate with id " + id + " found.");
        }
        dto.setId(id);
        dto.setDateEdited(LocalDate.now());
        Candidate entity = candidateMapper.convertToEntity(dto);
        return candidateMapper.convertToDTO(candidateRepository.save(entity));
    }

    @Override
    public CandidateDTO delete(Long id) throws EntityNotFoundException {
        Optional<Candidate> candidateOptional = candidateRepository.findById(id);
        if (!candidateOptional.isPresent()) {
            throw new EntityNotFoundException("No candidate with id " + id + " found.");
        }
        candidateRepository.deleteById(id);
        return candidateMapper.convertToDTO(candidateOptional.get());
    }

}
