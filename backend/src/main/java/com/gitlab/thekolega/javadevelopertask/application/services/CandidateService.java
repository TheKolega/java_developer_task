package com.gitlab.thekolega.javadevelopertask.application.services;

import com.gitlab.thekolega.javadevelopertask.application.dto.CandidateDTO;
import com.gitlab.thekolega.javadevelopertask.application.dto.CandidateDTOCreate;
import com.gitlab.thekolega.javadevelopertask.application.dto.CandidateDTOEdit;

import javax.persistence.EntityNotFoundException;
import java.util.List;

public interface CandidateService {

    /**
     * Create entity and return the created instance.
     *
     * @param dto entity to create
     * @return created entity
     */
    CandidateDTO create(CandidateDTOCreate dto);

    /**
     * Find entity with given id.
     *
     * @param id of the entity to find
     * @return entity, if it exists
     * @throws EntityNotFoundException if no entity with given id exists
     */
    CandidateDTO findOne(Long id) throws EntityNotFoundException;

    /**
     * Find all existing entities.
     *
     * @return list of all existing entities or empty list if no entities exist.
     */
    List<CandidateDTO> findAll();

    /**
     * Update entity with given id with the contents of a given entity.
     *
     * @param id  of the entity to update
     * @param dto entity with updated values
     * @return entity updated, if it exists
     * @throws EntityNotFoundException if no entity with given id exists
     */
    CandidateDTO update(Long id, CandidateDTOEdit dto) throws EntityNotFoundException;

    /**
     * Delete entity with given id.
     *
     * @param id of the entity to delete
     * @return entity deleted, if it exists
     * @throws EntityNotFoundException if no entity with given id exists
     */
    CandidateDTO delete(Long id) throws EntityNotFoundException;

}
