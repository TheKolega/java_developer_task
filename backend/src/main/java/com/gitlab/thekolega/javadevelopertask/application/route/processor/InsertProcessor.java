package com.gitlab.thekolega.javadevelopertask.application.route.processor;

import com.gitlab.thekolega.javadevelopertask.application.dto.CandidateDTOCreate;
import org.apache.camel.Exchange;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class InsertProcessor implements org.apache.camel.Processor {

    @Override
    public void process(Exchange exchange) throws Exception {

        //Take the Candidate object from the exchange and create the insert query
        CandidateDTOCreate dto = exchange.getIn().getBody(CandidateDTOCreate.class);
        String query = "INSERT INTO candidate(first_name, last_name, unique_citizen_id, year_of_birth, email, phone, comment, is_hired, date_edited)values('"
                + dto.getFirstName() + "','"
                + dto.getLastName() + "','"
                + dto.getUniqueCitizenId() + "','"
                + dto.getYearOfBirth() + "','"
                + dto.getEmail() + "','"
                + dto.getPhone() + "','"
                + dto.getComment() + "','"
                + dto.isHired() + "','"
                + LocalDate.now() + "')";
        // Set the insert query in body
        exchange.getIn().setBody(query);

    }

}
