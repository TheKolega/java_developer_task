package com.gitlab.thekolega.javadevelopertask.api.controllers;

import com.gitlab.thekolega.javadevelopertask.application.dto.CandidateDTO;
import com.gitlab.thekolega.javadevelopertask.application.dto.CandidateDTOCreate;
import org.apache.camel.ProducerTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/candidate-routes")
public class RouteController {

    @Autowired
    ProducerTemplate producerTemplate;

    @GetMapping(value = "/to-CSV")
    public void selectAllToCsv() {
        producerTemplate.requestBody("direct:selectToCsv", null, List.class);
    }

    @GetMapping
    public ResponseEntity<List<CandidateDTO>> selectAllCandidates() {
        List<CandidateDTO> candidateDTOList = producerTemplate.requestBody("direct:select", null, List.class);
        return new ResponseEntity<>(candidateDTOList, HttpStatus.OK);
    }

    @PostMapping(consumes = "application/json")
    public ResponseEntity<CandidateDTO> insertCandidate(@Valid @RequestBody CandidateDTOCreate dto) {
        producerTemplate.requestBody("direct:insert", dto, List.class);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

}