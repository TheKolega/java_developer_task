package com.gitlab.thekolega.javadevelopertask.domain;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(schema = "public")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Candidate {

    @Id
    //Sequence generator set to follow SERIAL default sequence naming "{tableName}_{columnName}_seq"
    @SequenceGenerator(name = "candidate_id_seq", sequenceName = "candidate_id_seq", allocationSize = 1, schema = "public")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "candidate_id_seq")
    @EqualsAndHashCode.Include
    private Long id;

    @Column(length = 64)
    private String firstName;

    @Column(length = 64)
    private String lastName;

    @Column(length = 16)
    private String uniqueCitizenId;

    @Column
    private Short yearOfBirth;

    @Column(length = 320)
    private String email;

    @Column(length = 16)
    private String phone;

    @Column(length = 255)
    private String comment;

    @Column
    private boolean isHired = false;

    @Column
    private LocalDate dateEdited = LocalDate.now();

}
