package com.gitlab.thekolega.javadevelopertask.api.controllers;

import com.gitlab.thekolega.javadevelopertask.application.dto.CandidateDTO;
import com.gitlab.thekolega.javadevelopertask.application.dto.CandidateDTOCreate;
import com.gitlab.thekolega.javadevelopertask.application.dto.CandidateDTOEdit;
import com.gitlab.thekolega.javadevelopertask.application.services.CandidateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/candidates")
public class CandidateController {

    @Autowired
    private CandidateService candidateService;

    @PostMapping
    public ResponseEntity<CandidateDTO> createCandidate(@Valid @RequestBody CandidateDTOCreate dto) {
        return new ResponseEntity<>(candidateService.create(dto), HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<CandidateDTO> findCandidate(@PathVariable Long id) {
        return new ResponseEntity<>(candidateService.findOne(id), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<CandidateDTO>> findAllCandidates() {
        return new ResponseEntity<>(candidateService.findAll(), HttpStatus.OK);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<CandidateDTO> updateCandidate(@PathVariable Long id, @Valid @RequestBody CandidateDTOEdit dto) {
        return new ResponseEntity<>(candidateService.update(id, dto), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<CandidateDTO> deleteCandidate(@PathVariable Long id) {
        return new ResponseEntity<>(candidateService.delete(id), HttpStatus.OK);
    }

}
