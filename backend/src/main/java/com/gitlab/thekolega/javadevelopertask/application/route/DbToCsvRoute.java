package com.gitlab.thekolega.javadevelopertask.application.route;

import com.gitlab.thekolega.javadevelopertask.application.route.processor.SelectProcessor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.dataformat.bindy.csv.BindyCsvDataFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;

@Component
public class DbToCsvRoute extends RouteBuilder {

    @Autowired
    DataSource dataSource;

    @Autowired
    SelectProcessor selectProcessor;

    private BindyCsvDataFormat bindy = new BindyCsvDataFormat(com.gitlab.thekolega.javadevelopertask.application.dto.CandidateDTO.class);

    @Override
    public void configure() throws Exception {

        // SelectToCsv Route
        from("direct:selectToCsv")
                .setBody(constant("select * from candidate order by id asc"))
                .to("jdbc:dataSource")
                .process(selectProcessor)
                .marshal(bindy)
                .to("file:src/main/resources/CSV?fileName=${date:now:yyyy-MM-dd}-Candidates.csv");

    }

}
