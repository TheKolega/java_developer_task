DROP TABLE IF EXISTS candidate CASCADE;
CREATE TABLE candidate(id BIGSERIAL PRIMARY KEY, first_name VARCHAR(64), last_name VARCHAR(64), unique_citizen_id VARCHAR(16), year_of_birth SMALLINT, email VARCHAR(320), phone VARCHAR(16), comment VARCHAR(255), is_hired boolean, date_edited DATE);
