# Java Developer Task

> Export data from a database to a .CSV file.

## Table of contents

- [General info](#general-info)
- [Technologies](#technologies)
- [Setup](#setup)
- [To do](#to-do)
- [Contact](#contact)

## General info

A small app designed to export a list of candidates for a certain position, from an SQL querry to a .CSV file.

## Technologies

- Java: 11
- SpringBoot: 2.4.5
- Apache Camel: 3.9.0

## Setup

- Build and run the back-end by navigating to `cd backend` and issuing the command `mvnw spring-boot:run`
- Stop the app using ctrl+c ( ^C )

<br>

- The server will be running on http://localhost:51711
- The test database is targeted on http://localhost:51712/javadevelopertaskdb with username `postgres` and
  password `postgres`
- Swagger UI showing all api endpoints is available on http://localhost:51711/swagger-ui.html
- Saving to CSV triggered on http://localhost:51711/api/candidate-routes/to-CSV
- CSV files by default saved in ./src/main/resources/CSV

## To Do:

- Web UI
- Choose destination for CSV files

## Contact

<a href="https://www.gitlab.com/TheKolega">
    <img src="https://img.shields.io/badge/GitLab-TheKolega-grey?style=for-the-badge&logo=GitLab&labelColor=548"/>
</a> &nbsp;
<a href="https://www.linkedin.com/in/milos-jov-rs">
    <img src="https://img.shields.io/badge/LinkedIn-Milo%C5%A1_Jovanovi%C4%87-grey?style=for-the-badge&logo=linkedin&labelColor=0A66C2"/>
</a>
